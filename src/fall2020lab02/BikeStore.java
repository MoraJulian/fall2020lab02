//Julian Mora Mesa
//Student id: 1733574
package fall2020lab02;

public class BikeStore {

	public static void main(String[] args) {
		
		Bicycle[] bikeStock = new Bicycle[4];
		bikeStock[0] = new Bicycle("Yamabruh",2,30);
		bikeStock[1] = new Bicycle("Mistubooly",4,80);
		bikeStock[2] = new Bicycle("PresidentsChoice",6,160);
		bikeStock[3] = new Bicycle("AMD",6,190);
		
		for(Bicycle i: bikeStock) {
			System.out.println(i);
		}
		
	}

}
