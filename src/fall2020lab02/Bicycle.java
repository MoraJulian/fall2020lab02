//Julian Mora Mesa
// Student id: 1733574
package fall2020lab02;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public String getManufacturer() {
		return this.manufacturer;
	}
	
	public int getNumberGears() {
		return this.numberGears;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public Bicycle(String newManufacturer,int newNumberGears, double newMaxSpeed) {
		this.manufacturer = newManufacturer;
		this.maxSpeed =  newMaxSpeed;
		this.numberGears = newNumberGears;
	}
	
	public String toString(){
		return "Manufacturer: " + this.manufacturer + "Number of Gears: " + this.numberGears + "Max speed: " + this.maxSpeed;
	}
	
}
